## ES6, Babel & Webpack project boilerplate
##### Installation

###### 1. Clone repository
Navigate to your dev folder (~/sites for example)
```sh
$ git clone https://dennis_verleg@bitbucket.org/dennis_verleg/es6-boilerplate.git
$ cd es6-boilerplate
```
###### 2. Install dependencies
Install dependencies using [Yarn](https://yarnpkg.com/)
```sh
$ yarn install
```
###### 3. Run webpack
This wil run webpack --watch (see package.json) which will watch your .js and .scss files in the ./assets folder
```sh
$ yarn run dev
```
You can also run the following command to compile your files once without watching them
```sh
$ yarn run webpack
```