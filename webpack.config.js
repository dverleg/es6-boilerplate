const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const webpackUglifyJsPlugin = require('webpack-uglify-js-plugin');

module.exports = {
	entry: './assets/scripts/main.js',
	output: {
		path: path.resolve(__dirname, 'build'),
		filename: 'bundle.js'
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				loader: 'babel-loader',
				query: {
					presets: ['es2015']
				}
			}, {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract('style', ['css', 'postcss', 'sass']),
                include: /assets\/styles/
            }
		]
	},
	plugins: [
		new ExtractTextPlugin("./css/style.css"),
		new webpackUglifyJsPlugin({
			cacheFolder: './cache/',
			debug: true,
			minimize: true,
			sourceMap: false,
			output: {
				comments: false
			},
			compressor: {
				warnings: false
			}
		})
	],
	postcss: [
		autoprefixer({
			browsers: ['last 3 versions']
		})
	],
	stats: {
		colors: true
	},
	devtool: 'source-map'
};